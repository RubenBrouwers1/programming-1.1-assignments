package M7.A14StudentContract.Person;

public class Student extends Person {
    private int number;

    public Student( int number, String name, String email, String mobileNumber, String fixedNumber) {
        super(name, email, fixedNumber, mobileNumber);
        this.number = number;
    }

    @Override
    public String toString() {
        return "studentNumber: " + number + super.toString();
    }
}
