package M7.Testing.A09Shape;

public class Circle extends Shape{

    int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public Circle(){}

    public Circle(int x, int y) {
        super(x, y);
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getArea() {
        return (int) (Math.PI*Math.pow(radius, 2));
    }

    public double getPerimeter(){
        return (int) (2*Math.PI*radius);
    }


}
