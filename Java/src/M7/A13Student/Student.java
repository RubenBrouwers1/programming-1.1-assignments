package M7.A13Student;

public class Student extends Person{
    int number;


    public Student(String name, String phone, int number) {
        super(name, phone);
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("Student number: %d, Student name: %s, Student phone number: %s", number, name, phone);
    }

}
