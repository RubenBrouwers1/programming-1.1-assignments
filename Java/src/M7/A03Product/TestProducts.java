package M7.A03Product;

import M7.A03Product.books.Book;
import M7.A03Product.clothes.Shirt;
import M7.A03Product.electronics.Camera;

public class TestProducts {
    public static void main(String[] args) {
        Product book = new Book("De nacht van 19 April", "Jan De Rijke", "12345", "Warbook", 15F );
        Product shirt = new Shirt("Male", "XL", "12345", "Astro World", 20F);
        Product camera = new Camera(15, "12345", "Sony A7x", 200F);

        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println(book);
        System.out.println(shirt);
        System.out.println(camera);
    }
}
