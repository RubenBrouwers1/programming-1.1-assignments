package M7.A07Dog;

public class Animal {
    private String tagLine;
    private String name;
    private String breed;
    private String colour;

    public Animal(String name, String breed, String colour, String tagLine) {
        this.name = name;
        this.breed = breed;
        this.colour = colour;
        this.tagLine = tagLine;
    }

    @Override
    public String toString() {
        return "The name of this animal is " + name + ", it is a '" + breed + "' and the color of it's fur is "
                + colour + ". The tagline for this animal is: '" + tagLine + "'. ";
    }
}


