package M7.A07Dog;

public class Rabbit extends Animal{
    private boolean digs;
    //private String isDigger;

    public Rabbit(String name, String breed, String colour, boolean digs) {
        super( name, breed, colour, "I'm an ice rabbit");
        this.digs = digs;
    }
/*
    public String isDigs(String isDigger) {
        if(digs = true) {
            isDigger = "is a digger";
        } else {
            isDigger = "is not a digger";
        } return isDigger;
    }
 */

    @Override
    public String toString() {
        return super.toString() + "It is " + digs + " that this rabbit is a digger";
    }
}
