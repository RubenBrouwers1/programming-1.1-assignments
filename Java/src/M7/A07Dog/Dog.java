package M7.A07Dog;

public class Dog extends Animal {
    private final String chipNumber;

    public Dog(String chipNumber, String name, String breed, String colour) {
        super(name, breed, colour,"Like a dog in a bowling game" );
        this.chipNumber = chipNumber;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    @Override
    public String toString() {
        return "This dog has "+ chipNumber + " as chipnumber. " + super.toString();
    }

}
