package M7.A12Shape3D;

import java.lang.Math.*;

public class Cube extends Shape3D{

    protected double edge = 1.0;

    public Cube() {}

    public Cube(String colour, double edge) {
        super(colour);
        this.edge = edge;
    }

    public Cube(double edge) {
        this.edge = edge;
    }

    public double surface() {
        return 6 * Math.pow(edge, 2);
    }

    public double volume() {
        return Math.pow(edge, 3);
    }
}
