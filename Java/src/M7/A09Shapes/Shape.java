package M7.A09Shapes;

public abstract class Shape {
    protected int x;
    protected int y;


    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    protected Shape() {
    }

    public abstract double getArea();
    public abstract double getPerimeter();

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {

        String name = getClass().toString();
        name = name.substring(name.lastIndexOf('.') + 1);

        return String.format("%s at (%d, %d) with perimeter %.2f and surface %.2f",
                name, x, y, getPerimeter(), getArea());

    }

}
