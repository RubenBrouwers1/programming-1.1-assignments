package M6.Solutions.A09_array_of_characters.src.be.kdg.prog1;

public class ArrayOfCharacters {
    public static void main(String[] args) {
        String word = "JavaScript";

        char[] letters = word.toCharArray();

        for (char letter : letters) {
            System.out.print(letter + " ");
        }
    }
}
