package M6.A01ArraysV1;

public class ArraysV1 {
    public static void main(String[] args) {
        int[] numbers = new int[5];
        float[] stockMarketRates = new float[20];
        boolean[] switches = new boolean[8];
        String[] words = new String[4];

        System.out.println(numbers[0]);
        System.out.println(stockMarketRates[0]);
        System.out.println(switches[0]);
        System.out.println(words[0]);

        System.out.println(" ");

        System.out.println(numbers[(numbers.length)-1]);
        System.out.println(stockMarketRates[(stockMarketRates.length)-1]);
        System.out.println(switches[(switches.length)-1]);
        System.out.println(words[(words.length)-1]);
    }
}