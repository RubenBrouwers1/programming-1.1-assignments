package M6.Testing.A10PlayingCards;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();
        int cardCount = 0;
        Card[] deck = new Card[52];

        String[] suits = {
                "clubs", "diamonds", "spades", "hearts"
        };
        String[] ranks = {
                "Ace", "Two", "Three", "Four", "Five",
                "Six", "Seven", "Eight", "Nine", "Ten",
                "Jack", "Queen", "King"
        };

        boolean correctInput = false;
        do {
            System.out.print("How many cards would you like? (1..5): ");
            cardCount = sc.nextInt();
            if (cardCount > 5 || cardCount < 1) {
                System.out.println("Your input is out of range, please try again!");
                correctInput = false;
            } else {
                correctInput = true;
            }
        } while (!correctInput);

        int deckPlace = 0;
        for (String suit : suits) {
            for (String rank : ranks) {
                deck[deckPlace] = new Card(rank, suit);
                deckPlace++;
            }
        }

        for (int i = 1; i <= cardCount; i++) {
            int selectedCard = rd.nextInt(deck.length);
            System.out.println(deck[selectedCard].getRank() + " of " +  deck[selectedCard].getSuit());
        }
    }
}

