package M6.Testing.A06CountingLetters;

import java.lang.reflect.Array;
import java.util.Locale;
import java.util.Scanner;

public class CountingLetters {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[] lettercount = new int[26];
        int charcount = 0;
        String sentence;

        System.out.print("Please enter a sentence: ");
        sentence = sc.nextLine().toLowerCase();

        for (int i = 1; i < sentence.length(); i++){
            char c = sentence.charAt(i);
            if(c > 97 && c < 123) {
                lettercount[c - 97]++;
                charcount++;
            }
        }

        for (char c = 97, j = 0; j < lettercount.length; j++, c++){
            System.out.printf("%c --> %d times \t%s", c, lettercount[j], ((j+1)%4 ==0) ? "\n" : " ");
        }
    }
}


