package M6.SolutionsRoby;

import java.util.Scanner;

public class A05 {
    public static void main(String[] args) {
        int numberOfTemperatures = 2;
        float[] temperatures = new float[numberOfTemperatures];
        float average = 0;

        Scanner sc = new Scanner(System.in);

        //ask temperatures
        for(int i = 0; i < numberOfTemperatures; i++){
            System.out.print("Day " + (i+1) + ": ");
            temperatures[i] = sc.nextFloat();
            average += temperatures[i];
        }

        //average = sum / total
        average /= temperatures.length;
        //create table
        System.out.println("Summary: \n===============");
        for(int i = 0; i < numberOfTemperatures; i++){
            //fill in 10 characters -> create space if not filled
            System.out.printf("%-10s " + temperatures[i], "Day " + (i + 1) + ": ");
            System.out.println();
        }
        System.out.println("===============");
        //%.2f = 2 decimals after comma
        System.out.printf("Average: %.2f ", average);
    }
}
