package M8.Solutions.A03_animals.src.be.kdg.prog1.a803;

public interface Named {
    String getName();
}
