package M8.A03Animals;

public interface EggLaying {
    int getNumberOfEggsPerYear();
}
