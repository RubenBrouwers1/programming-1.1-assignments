package M8.A02Comparable;

public class Car implements Printable, Comparable {
    private String brand;
    private String model;
    private String licensePlate;

    public Car(String brand, String model, String licensePlate) {
        this.brand = brand;
        this.model = model;
        this.licensePlate = licensePlate;
    }

    @Override
    public void print() {
        System.out.printf(
                "Car\n" +
                "===\n" +
                "Brand:         %s\n" +
                "Model:         %s\n" +
                "License plate: %s\n", brand, model, licensePlate);
    }


    @Override
    public boolean isEqualTo(Object o) {
        if (o instanceof Car) {
            Car other = (Car) o;
            return brand.equals(other.brand) && model.equals(other.model);
        } else {
            return false;
        }
    }

    @Override
    public boolean isGreaterThan(Object o) {
        if (o instanceof Car) {
            Car other = (Car) o;
            if (brand.equals(other.brand)) {
                return model.compareTo(other.model) > 0;
            } else {
                return brand.compareTo(other.brand) > 0;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean isLessThan(Object o) {
        if (o instanceof Car) {
            Car other = (Car) o;
            if (brand.equals(other.brand)) {
                return model.compareTo(other.model) < 0;
            } else {
                return brand.compareTo(other.brand) < 0;
            }
        } else {
            return false;
        }
    }
}
