package M8.A02Comparable;

public class Circle extends Shape {
    int radius;

    public Circle (int x, int y, int radius) {
        super(x,y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * (radius * radius);
    }
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void print() {
        System.out.printf("" +
                "Circle\n" +
                "======\n" +
                "Position: (%d, %d)\n" +
                "Radius:   %d\n", getX(), getY(), radius);
    }


    @Override
    public boolean isEqualTo(Object o) {
        if (o instanceof Circle) {
            Circle other = (Circle) o;
            return radius == other.radius;
        } else {
            return false;
        }
    }

    @Override
    public boolean isGreaterThan(Object o) {
        if (o instanceof Circle) {
            Circle other = (Circle) o;
            return radius > other.radius;
        } else {
            return false;
        }
    }

    @Override
    public boolean isLessThan(Object o) {
        if (o instanceof Circle) {
            Circle other = (Circle) o;
            return radius < other.radius;
        } else {
            return false;
        }
    }
}
