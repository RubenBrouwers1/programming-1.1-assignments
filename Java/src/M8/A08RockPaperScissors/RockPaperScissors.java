package M8.A08RockPaperScissors;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        System.out.println();
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();

        int userInput = 0;
        int quitGame;
        boolean userInputCor = false;
        final String options[] = {"Rock", "Paper", "Scissors"};

        System.out.println("This is a game of \"Rock Paper Scissors\"");
        System.out.println("Rock = 1, Paper = 2, Scissors = 3");
        System.out.println("------");

        while (true){
            System.out.print("Rock(1), paper(2) or scissors(3)? (Enter 0 to stop): ");
            do {
                try {
                    userInput = sc.nextInt();
                    quitGame = userInput;
                    if (quitGame == 0) {
                        System.exit(69);
                    }else if (userInput > 3 || userInput < 0) {
                        userInputCor = false;
                        System.out.print("Enter 0, 1, 2 or 3: ");
                    } else if (userInput <= 3 || userInput >= 1) {
                        userInputCor = true;
                    }
                } catch (InputMismatchException e) {
                    userInputCor = false;
                    System.out.print("Enter 0, 1, 2 or 3: ");
                    sc.next();
                }
            }while (!userInputCor);

            int compInput = rd.nextInt(2);

            System.out.printf("Your choice: %s\n", options[userInput - 1]);
            System.out.printf("My choice : %s\n", options[compInput]);

            if (userInput - 1 == compInput) {
                System.out.println("\tIt's a tie\n");
            } else if ((userInput - 1 == 0 && compInput == 2) || (userInput - 1 == 1 && compInput == 0) || (userInput - 1 == 2 && compInput == 1)) {
                System.out.println("\tCongratulations, You won!\n");
            } else if ((userInput - 1 == 0 && compInput == 1) || (userInput - 1 == 1 && compInput == 2) || (userInput - 1 == 2 && compInput == 0)) {
                System.out.println("\tI won!\n");
            }
        }
    }
}
