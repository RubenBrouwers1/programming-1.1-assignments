package M8.A05PointsV2;

import java.util.InputMismatchException;

public class PointDemo {
    public static void main(String[] args) {
        System.out.println("Count: " + Point.getCount());
        Point p1 = new Point();
        try {
            System.out.println(p1);
        System.out.println(Point.getCOLOR() + " " + Point.getCount());
        Point p2 = new Point(5, 5);
            System.out.println(p2);
        System.out.println(Point.getCOLOR() + " " + Point.getCount());
        } catch (InputMismatchException e) {
            System.out.println("Null pointer in point " + e);
            e.printStackTrace();
        } catch (Exception e) {

        }
        System.out.println("Ending normally");
    }
}
