package M3.A09Interest;

import java.util.Scanner;

public class Interest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double StartingCapital;
        double InterestRate;
        double EndingCapital =0;

        int numY = 0;
        int i =0;

        System.out.print("What is your starting capital?: ");
        StartingCapital = sc.nextDouble();


        System.out.print("What is your interest rate?: ");
        InterestRate = 1 + (sc.nextDouble()/100);
        System.out.print("For how many years would you like to save?: ");
        numY = sc.nextInt();
/*
        do {
            EndingCapital = EndingCapital * ((InterestRate/10)+1);
            i++;

        } while(i<=numY);
*/
        EndingCapital = StartingCapital;
        for (i = 1; i <= numY; i++) {
            EndingCapital *= InterestRate; //I did something wrong here!
        }

        System.out.println("Your capital will amout to: " + (long)EndingCapital);

//This I copied from the solution
        double factor  = InterestRate;
        while (factor < 2) {
            numY++;
            factor *= InterestRate ;
        }
        System.out.println("It takes " + numY + " years to double the money." );
    }
}
