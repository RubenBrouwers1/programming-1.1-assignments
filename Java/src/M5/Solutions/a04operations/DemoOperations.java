package M5.Solutions.a04operations;

import java.util.Scanner;

public class DemoOperations {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Please enter the first integer: ");
        int eerste = keyboard.nextInt();
        System.out.print("Please enter the second integer: ");
        int tweede = keyboard.nextInt();

        Operations operations = new Operations(eerste, tweede);

        System.out.printf("The sum is %d\n", operations.sum());
        System.out.printf("The difference is %d\n", operations.difference());
        System.out.printf("The product is %d\n", operations.product());
        System.out.printf("The quotiënt is %.2f\n", operations.quotient());
    }

}
