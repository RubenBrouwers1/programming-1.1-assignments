package M5.Solutions.a07circle;

/**
 * @author Kristiaan Behiels
 * @version 1.0 12/10/2014 15:26
 */
public class DemoCircle {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        Circle small = new Circle(10, "Red");
        Circle big = new Circle(11, "Blue");

        System.out.println(circle);
        System.out.println(small);
        System.out.println(big);

        System.out.printf("Difference in circumference (red - blue): %.2f%n", small.deltaCircumference(big));
        System.out.printf("Difference in surface (blue - red): %.2f%n", big.deltaSurface(small));

    }
}

/*
Kleur: Zwart
Straal: 5
Omtrek: 31,42
Oppervlakte: 78,54

Kleur: Rood
Straal: 10
Omtrek: 62,83
Oppervlakte: 314,16

Kleur: Blauw
Straal: 11
Omtrek: 69,12
Oppervlakte: 380,13

Verschil omtrek (rood - blauw): 6,28
Verschil oppervlakte (blauw - rood): 65,97
 */