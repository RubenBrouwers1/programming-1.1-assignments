package M5.Testing.A04Operations;

import java.util.Scanner;

public class DemoOperation {
    public static void main(String[] args) {
        Scanner sc =  new Scanner(System.in);

        double numberOne, numberTwo;

        System.out.print("Please enter the first number: ");
        numberOne = sc.nextDouble();
        System.out.print("Please enter the second number: ");
        numberTwo = sc.nextDouble();

        Operations operations = new Operations(numberOne, numberTwo);
        System.out.println("The sum is = " + operations.sum());
        System.out.println("The difference is = " + operations.difference());
        System.out.println("The product is = " + operations.product());
        System.out.println("The quotient is = " + operations.quotient());
    }
}
