package M5.Testing.A03Box;

public class Box {
    private String type;
    private double length;
    private double width;
    private double height;

    public Box(String type, double length, double width, double height) {
        this.type = type;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public Box(String type, double length) {
        this(type, length, 0, 0);
    }

    public double surface(){
        return 2*(height*length)+2*(width*length)+2*(height*width);
    }

    public double volume(){
        return width*height*length;
    }

    public double tapeLength(){
        return 2*(height+length);
    }

    @Override
    public String toString() {
        return  String.format("Type: %s\n" +
                "Length: %.2f\n"+
                "Width: %.2f\n"+
                "Height: %.2f\n"+
                "Surface: %.2f\n"+
                "Volume: %.2f\n"+
                "Minimum tapelength: %.2f\n", type,  length, width, height, surface(), volume(), tapeLength());

    }
}
