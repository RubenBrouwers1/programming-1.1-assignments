package M2.Solutions.A08_ASCII_values.src.be.kdg.java1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string: ");
        int sum = 0;
        String text = keyboard.nextLine();
        for (char c : text.toCharArray()) {
            System.out.printf("%c has an ASCII value of %d\n", c, +c);
            sum += c;
        }
        System.out.println(sum);
    }
}
