package M2.Solutions.A14_encoding.src.be.kdg.java1;

import java.util.Scanner;

public class Main {
    /*
    Enter a message: The quick brown fox jumps over the lazy dog
    Displacement to be used: 6
    Encoded message:
    ZNK WAOIQ HXUCT LUD PASVY UBKX ZNK RGFE JUM
    Decoded message:
    THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
    */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a message: ");
        String message = scanner.nextLine().toUpperCase();
        System.out.print("Displacement to be used: ");
        int displacement = scanner.nextInt();
        System.out.println("Encoded message:");
        String encoded = "";
        for (char c : message.toCharArray()) {
            if (c < 65 || c > 90) {
                encoded += c;
            } else {
                encoded += (char)((c - 65 + displacement) % 26 + 65);
            }
        }
        System.out.println(encoded);
        System.out.println("Decoded message:");
        String decoded = "";
        for (char c : encoded.toCharArray()) {
            if (c < 65 || c > 90) {
                decoded += c;
            } else {
                decoded += (char)((c - 65 - displacement + 26) % 26 + 65);
            }
        }
        System.out.println(decoded);
    }
}
