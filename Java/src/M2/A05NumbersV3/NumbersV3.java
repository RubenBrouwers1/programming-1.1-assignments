package M2.A05NumbersV3;

import java.util.Scanner;

public class NumbersV3 {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        final long MINIMUM_DIVIDEND = 1000000000000L;
        final long MINIMUM_DIVISOR = 10000000L;
        long num1;
        long num2;
        long answer;

        System.out.print("Please enter a 13-digit number: ");
        num1 = keyboard.nextLong();
        if (num1 < MINIMUM_DIVIDEND) {
            System.out.println("This number is to small");
        }

        System.out.print("Please enter an 8-digit number: ");
        num2 = keyboard.nextLong();
        if (num2 < MINIMUM_DIVISOR) {
            System.out.println("This number is to small");
        }
        answer = num1 / num2;
        System.out.print("The answer is: " + answer);
        return;
    }
}
