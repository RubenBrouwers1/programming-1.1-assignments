package M1.Solutions.A09_BMIv2.src.be.kdg;

import java.util.Scanner;

public class Bmi {

	public static void main(String[] args) {
		//declarations:
		Scanner keyboard = new Scanner(System.in);
		int weight;
		double length;
		double bmi;

		//input:
		System.out.println("Dear patient, this programma will calculate your BMI.");
		System.out.print("Enter your weight in kilograms: ");
		weight = keyboard.nextInt();
		System.out.print("Enter your length in meters: ");
		length = keyboard.nextDouble();
		//calculate and display:
		bmi = (weight / (length * length))*10000;
		System.out.println("Your BMI is: " + bmi);
		//advise
		System.out.print("Your BMI class is: ");
		if (bmi < 18) {
			System.out.println("underweighted.");
		} else if (bmi < 25) {
			System.out.println("healthy weight.");
		} else if (bmi < 30) {
			System.out.println("overweighted.");
		} else {
			System.out.println("obese.");
		}
	}
    public static void masin(String[] args) {
        //declaraties:
        Scanner keyboard = new Scanner(System.in);
        int gewicht;
        double lengte;
        double bmi;

        //input:
        System.out.println("Beste patient, dit programma berekent je BMI.");
        System.out.print("Geef je gewicht in kilogram: ");
        gewicht = keyboard.nextInt();
        System.out.print("Geef je lengte in meters: ");
        lengte = keyboard.nextDouble();
        //berekening en output:
        bmi = gewicht / (lengte * lengte);
        System.out.println("Je BMI is: " + bmi);
        //Uitbreiding: advies
        System.out.print("Dat is: ");
        if (bmi < 18) {
            System.out.println("ondergewicht!");
        } else if (bmi < 25) {
            System.out.println("in orde!");
        } else if (bmi < 30) {
            System.out.println("overgewicht!");
        } else {
            System.out.println("obesitas!");
        }
    }
}
