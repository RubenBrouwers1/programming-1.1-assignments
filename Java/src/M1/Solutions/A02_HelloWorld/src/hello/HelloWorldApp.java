/* This Java application shows
the text 'Hello World!' on the screen. */


package M1.Solutions.A02_HelloWorld.src.hello;


public class HelloWorldApp {
	public static void main(String[] args) {
		System.out.println("Hello World!"); // Show the text
	}
}
