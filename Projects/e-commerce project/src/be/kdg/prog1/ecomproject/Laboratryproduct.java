package be.kdg.prog1.ecomproject;

import java.util.Objects;

public class Laboratryproduct extends IndustrialAndScienceProduct implements Taxable{
    private final boolean useWater;
    private final String endGoal;

    public Laboratryproduct(boolean isElectric, String environment, boolean humanUse, boolean reUsable, float price,
                            String name, String description, boolean useWater, String endGoal) {
        super(isElectric, environment, humanUse, reUsable,  price, name, description);
        this.useWater = useWater;
        this.endGoal = endGoal;
    }

    public boolean isUseWater() {
        return useWater;
    }

    public String getEndGoal() {
        return endGoal;
    }

    @Override
    public String toString() {
        String LabProd = "useWater= %b, endGoal= %s";
        return super.toString() + String.format(LabProd, useWater, endGoal);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Laboratryproduct)) return false;
        if (!super.equals(o)) return false;
        Laboratryproduct laboratryProduct = (Laboratryproduct) o;
        return isUseWater() == laboratryProduct.isUseWater() && getEndGoal().equals(laboratryProduct.getEndGoal());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isUseWater(), getEndGoal());
    }
}
